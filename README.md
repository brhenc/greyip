# GreyIP

GreyIP is a hack to allow updating a dnsmasq DNS entry for a dynamic IP address as
soon as it changes. GreyIP is implemented in C as practice and to achieve high
efficiency and low latency. 

GreyIP is an optimization of an ansible script that achieves the same result,
however, the ansible script is not a daemon and it's terribly slow compared to
what needs to be achieved (cron resolution is 1 min, and the ansible script needs at least 15s to run. Latency is <100ms in my case). The general algorithm for the GreyIP implementation is the following:

Client process:
```
1) figure out the IP of the server as soon as possible
2) compare it to the last known IP address of the server
3) if they match, go back to 1)
4) if they don't match, send and encrypted tcp or udp packet
to the remote server daemon.
5) once sent, the client waits for a reply from the server that
the IP has been successfully updated.
 - if the update has been successful, store the new IP and go back to 1)
 - if the server sends an error reply, log the error and/or send an
 email/alert
 - if there's no reply, apply the policy defined in the config file
 (retry sending a few times, switch to tcp, log an error message, apply
 a back-off timeout). log a warning, for retries, and log an error as configured
 - if the server sends a back-off message, respect the requested back-off
 timeout. log a warning

- respond to server process health checks as soon as possible, if the server
process stops sending health checks, log error message and/or email/ send an
alert. Create a statistical model of the client - server latency that allows faster
diagnosis of a failure condition
```
Server process:
```
1) ping the client process on the last known IP address, if it doesn't respond,
and statistics show that a packet should have returned, log an error message and/or
send an email/send an alert.
2) Wait until an encrypted packet is received containing the DNS record of the client process and the associated new IP address + timestamp. Compare the received IP and timestamp to the last known IP address. If they match, do nothing. 
3) If the received IP address is different from the current one, update the DNS file
(by default) and/or run the user-specified process in a separate shell. Make sure that the update is successful, if it's not, log an error message and send the client an encrypted packet back specifying an error condition.
4) If the update was successful, send a success reply and asynchronously log an acknowledgment. Create a warning/error if the client fails to acknowledge.
```
TODO Features and nice-to-haves:
```
TCP/UDP (or maybe even QUIC) implementation with symmetric encryption (AES). Message should fit into a single UDP packet.
IPv4/IPv6 support
consensus-based algorithm for determining IP address of the client (multiple IP
sources are less likely to be compromised)
client support for multicasting (sending UDP packets to multiple servers)
event-based architecture (spamming packets over a network should be handled efficiently)
support for syslog logging, JSON logging directly to elasticsearch, sending emails, and alerting directly over the prometheus alertmanager API
JSON configuration file format, possibly toml
of course, client/server should be daemon processes
consider using HMAC for encryption (see RFC2104)
create a statically-compiled version and package it as a buildah/docker container
application should preferably be compilable by both gcc and llvm 
application should preferably be compilable with openssl/libressl
application should preferably be platform-independent (it should at a minimum work on every Linux distro and also on Raspberry PIs. 
application should preferably have a low-latency high-availability globally distributed IP resolution tool with \<Xms of latency globally (hint: use BGP) and possibly
a haproxy proxying solution (if abuse of the service can be kept to a minimum)
batteries-included philosophy should guide the design
```

Better design: as soon as the client sends a request for a new IP address, switch
to it proactively on the server and wait for asynchronous acknowledgment. UDP packets
are preferred in this case, to avoid the overhead of a triple-way handshake. Spamming
UDP packets. Considering that a minimal UDP packet can be assumed to be 576bytes, and
if we are transmitting every 1ms (in an extreme case), we're looking at a transfer of 49766400KB, or about 50GB/day, 1.5TB/month. Transmitting every 100ms, cuts down the value to 0.5GB/day and 15GB/month. Considering the price of 1TB of transfers/month is about 5USD on a VPS, it's safe to assume that a UDP packet sent every 3-100ms will easily consume at a maximum 500GB/month.

This problem of course wouldn't exist at all if you could get a static IP address for the price of a VPS droplet (5USD, or less).
